import { HttpClient } from '@angular/common/http';
import { Injectable, RootRenderer } from '@angular/core';
import { Observable } from 'rxjs';
import { Actor } from './actor';

@Injectable({
    providedIn: 'root'
})
export class ActorsService {

    constructor(private http: HttpClient) {

    }

    getActors(): Observable<any> {
        return this.http.get<any>('https://harmonodram.herokuapp.com/employees');
    }
}