import { Component, OnInit } from '@angular/core';
import { ActorsService } from './actors-service';
import { Actor } from './actor';

@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.css']
})
export class ActorsComponent implements OnInit {

  actors: any[];

  constructor(private actorsService: ActorsService) { }

  ngOnInit() {
    this.actorsService.getActors().subscribe(e => this.actors = e.employees)
  }

}
