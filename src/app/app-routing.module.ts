import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayFormComponent } from './play-form/play-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PlaysComponent } from './plays/plays.component';
import { ActorsComponent } from './actors/actors.component';

const routes: Routes = [
  {
    path: 'add-play',
    component: PlayFormComponent
  },
  {
    path: 'actors',
    component: ActorsComponent
  },
  {
    path: 'plays',
    component: PlaysComponent
  },
  {
    path: '',
    component: DashboardComponent

    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
