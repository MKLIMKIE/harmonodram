import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatToolbarModule, MatMenuModule, MatFormField, MatFormFieldModule, MatSelectModule, MatInputModule, MatCheckboxModule, MatIconModule, MatRadioGroup, MatRadioModule, MatStepperModule, MatCardModule} from '@angular/material';
import { PlayFormComponent } from './play-form/play-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActorsComponent } from './actors/actors.component';
import { PlaysComponent } from './plays/plays.component';
import { HttpClientModule } from '@angular/common/http';
import { PlayService } from './plays/play-service';

@NgModule({
  declarations: [
    AppComponent,
    PlayFormComponent,
    DashboardComponent,
    ActorsComponent,
    PlaysComponent
  ],
  imports: [
    MatCardModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule
  ],
  providers: [PlayService],
  bootstrap: [AppComponent]
})
export class AppModule { }
