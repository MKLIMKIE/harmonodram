import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Play } from './play';

@Injectable()
export class PlayService {
    constructor(private http: HttpClient) {

    }

    getAllPlays(): Observable<any> {
        return this.http.get<any>('https://harmonodram.herokuapp.com/plays');
    }

    getSinglePlay(title: string): Observable<any> {
        return this.http.get<any>(`https://harmonodram.herokuapp.com/plays/${title}`)

    }
}