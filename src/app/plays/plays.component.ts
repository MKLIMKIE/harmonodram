import { Component, OnInit } from '@angular/core';
import { PlayService } from './play-service';

@Component({
  selector: 'app-plays',
  templateUrl: './plays.component.html',
  styleUrls: ['./plays.component.css']
})
export class PlaysComponent implements OnInit {

  allPlays: any[];

  constructor(private playService: PlayService) { }

  ngOnInit() {

    this.playService.getAllPlays().subscribe(e => this.allPlays = e.play)
  }

  displaySinglePlay(title: any) {
    return this.playService.getSinglePlay(title).subscribe();

  }

}
